	#include "sten.h"

	#define HEADERSIZE 512 //the header size in bytes
	#define BYTESIZE   8   //bytes have 8 bits..
	#define BITINFOSIZE 3  //we need 3 bits to encode a number one through 8
	
	int main(int argc, char** argv){

		int num_bits; //number of bits to change,unsigned because we need to use the bit level encoding later on.

		FILE *pfp; //picture file pointer
		FILE *dfp; //data file pointer 
		FILE *ofp; //output file pointer

		//OpenFile returns false if something goes wrong
		if(!open_files(argc , argv, &pfp, &dfp, &ofp)){
			printf("exiting!\n");
			return 1;
		}

		//open_file ensure we have 4 arguments.
		//TODO: validate input. 
		num_bits = atoi(argv[4]);

		if(num_bits > 8){
			printf("error, number of bits (%i) cannot exceed 8\n", num_bits);
			printf("exiting!\n");
			return 1;
		}

		if(!parse_rgb_header(pfp)){
			printf("exiting!\n");
			return 1;
		}

		write_data(pfp, dfp, ofp,num_bits);

	} 


	//-----------------------------------------------------------------
	//OpenFile: opens the file, prints some error messages if there are too many
	// arguments or if there is an error opening the file

	int open_files(int argc, char *argv[], FILE **pfpp, FILE **dfpp, FILE **ofpp){
		if(argc != 5){
			printf("ERROR: wrong number of arguments. Usage: sten <Picture File> <data>  <output file name> <number of bits to change>\n");
			return 0;
		}

		*pfpp = fopen(argv[1], "r");  //We'll be reading and writing from the picture
		if(errno > 0){
			printf("Invalid picture file %s\n" , argv[1]);
			return 0;
		}


		*dfpp = fopen(argv[2], "r");  //We'll only need to read from the data file. 
		if(errno > 0){
			printf("Invalid data file %s\n" , argv[2]);
			return 0;
		}

		*ofpp = fopen(argv[3], "w");
		if(errno > 0){
			printf("something went wrong creating the output file. %s\n" , argv[2]);
			return 0;
		}


		return 1;
	}



	//-----------------------------------------------------------------
	//parse header: TODO: Right now this just makes sure the file is is at least 512 bytes long, eventually I may actually parse the header for information
	//this will return the file pointer 512 bytes from the start, which is nominally the beginning of an rgb file
	//IMPORTANT: sgi/rgb files are big-endian while the x86 processor that I'll assume this program will run on is little endian. I do everything one byte at a time this
	//doesn't affect anything, so that's what I've done here


	int parse_rgb_header(FILE *pfp){
		char c;
		int char_cnt = 0;
		
		//every call to fgetc moves the file pointer forward. 
		if(fgetc(pfp) != 1 || fgetc(pfp) != 0xda){
			printf("error: magic number != 01da\n");
			return 0;
		}
		printf("magic number found (dec: 474)\n");
	

		//the next byte tells us if the file is run length encoded, which I think is a form of compression. We won't support that n this program 
		c = fgetc(pfp);
		if(c == 0){
			printf("file is not RLE\n");
		}
		else if(c == 1){
			printf("file is RLE, this program won't decode that for you, sorry. try again with the uncompressed image\n");
			return 0;
		}
		else{
			printf("error parsing RLE bit, found value of %x\n", c);
			return 0;
		}

		//rgb headers are 512 bytes long, we've read in three bytes already  so we subtract those off. 
		while(char_cnt < HEADERSIZE - 3 && !feof(pfp)){
			getc(pfp); 
			char_cnt++;
			//printf("%x\n" , c);
		}   
		if(char_cnt != HEADERSIZE - 3){
			printf("invalid file size (total size == %i bytes) rgb headers must be at least 512 bytes, so somethings wrong here \n" , char_cnt);
			return 0;
		}

		printf("header parsed successfully (lets hope)\n");
		return 1;

	}


	//-----------------------------------------------------------------
	//write data
	int write_data(FILE *pfp, FILE *dfp, FILE *ofp, int num_bits){
	//the rgb file pointer should be handed to us 512 bytes removed from the start, although in this case the get length call returns 
	//the file pointer to zero anyway.
		int  size_p;
		int  size_d;
		int  byte_count;
		char mask;
		char pic_byte;
		char out_byte;
		char tmp_val;
		char p_data;
		int i = 0;

		//int num_fit = BYTESIZE / num_bits;
		int remainder = BYTESIZE % num_bits;

		size_p = (get_length(pfp) - 512); //minus 512 to account for the header (which we don't want to touch) 
		fseek(pfp, 512 , SEEK_SET); //returns file pointer to the end of the header

		size_d = get_length(dfp);

		printf("picture size (in bytes): %i\n", size_p);
		printf("message bits per pixel byte: %i\n" , num_bits);
		printf("bytes available for message: %i\n", size_p*num_bits/BYTESIZE);
		printf("message size: %i\n", size_d);

		if((size_p*num_bits/BYTESIZE) < size_d){
			printf("error! message size to large to fit in the picture: try increasing bit size, a larger picture, or compressing your message\n");
			return 0;
		}

		printf("every thing's set up, let's dance\n");
		copy_file(pfp, ofp);
		printf("picture data copied\n");

		//copy_file returns the file pointer to the start, so we need to move it back. 
		fseek(ofp, 512 , SEEK_SET);
		fseek(pfp, 512 , SEEK_SET);


		pic_byte = fgetc(pfp);
		out_byte = set_bits(tmp_val, num_bits, BITINFOSIZE);
		fputc(out_byte, ofp);

		byte_count = 0;
		while(byte_count  <  size_d){
			i = 0;

			while (i < (BYTESIZE / num_bits)){
				pic_byte = fgetc(pfp);
				out_byte = set_bits(pic_byte, pic_byte, num_bits);
				fputc(out_byte,ofp);
				pic_byte = pic_byte << num_bits; 

				i++;
				byte_count++;
			}
		}

		fclose(pfp);
		fclose(ofp);

		printf("all done!\n");

		return 0;

	}



	//-----------------------------------------------------------------
	//get_length: returns length of file in bytes, restarts the file to the beginning before getting length and returns it to the beginning 
	//when it's done
	int get_length(FILE *fp){
		int length = 0;
	    fseek(fp, 0 , SEEK_SET); //returns file pointer to the beginning 
	    while(!feof(fp)){
	    	fgetc(fp);
	    	length++;
	    }
	     fseek(fp, 0 , SEEK_SET); //returns file pointer to the beginning 
	     return length;
	}

	//-----------------------------------------------------------------
	//set bits: returns the character o_value with the last bits changed. the num_bits parameter specifies how many bits to change,
	//the bits parameter are the new bits to put in the least significant bits.
	char set_bits(char o_value, char new_bits, char num_bits){
		char mask = 0xff << num_bits;
		new_bits = ~mask | new_bits; //make sure the new bits are zero everywhere except what we're changing. 
		return (o_value & mask) | (new_bits);

	}

	//-----------------------------------------------------------------
	//copy file: makes an exact copy of the file we opened for output
	void copy_file(FILE *ifp, FILE *ofp){

		fseek(ofp, 0 , SEEK_SET);
		fseek(ifp, 0 , SEEK_SET);

		while(!feof(ifp)){
			fputc(fgetc(ifp), ofp);
			
		}
		//return both pointers to start of file

		fseek(ofp, 0 , SEEK_SET);
		fseek(ifp, 0 , SEEK_SET);

		return;

	}

