#include "stdio.h"
#include "errno.h"


int open_files(int argc, char *argv[], FILE **dfpp, FILE **pfpp, FILE **ofpp);
int parse_rgb_header(FILE *pfp);
int write_data(FILE *pfp, FILE *dfp, FILE *ofp, int num_bits);
int get_length(FILE *fp);
char set_bits(char value, char bits, char shift);
void copy_file(FILE *ifp, FILE *ofp);